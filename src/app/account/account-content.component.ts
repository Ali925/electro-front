import { Component, Input, DoCheck, OnInit, ElementRef, IterableDiffers, IterableDiffer } from '@angular/core';
import { ElectroService } from '../electro.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/Rx';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-account-content',
  templateUrl: './account-content.component.html',
  styleUrls: ['./account-content.component.css']
})

export class AccountContentComponent implements OnInit {
	
		@Input() brends:any;
		@Input() models:any;
		@Input() blogs:any;
	
		regUser = {
				name: '',
				surname: '',
				email: '',
				password: ''
			};
		signUser = {
				email: '',
				password: '',
				remember: false
		};
	
		lostUser = {
				email: ''
		};
	
		successRes = false;
		successActive =false;
		resendLink = false;
		resendSuccess = false;
		lostPassword = false;
	
		showSigninError = false;
		showEmailError = false;
		signinError = '';
	
		showSignupError = false;
		showUpEmailError = false;
		signupError = '';
	
		showLostError = false;
		lostError = '';
		lostSuccess = false;
		showLostForm = true;
	
		constructor(private route: ActivatedRoute, private eService: ElectroService, private cookieService: CookieService) {}
	
		ngOnInit() {
			let self = this;
			console.log(this.route);
			
			if((self.route.queryParams as any).value.activate == "true"){
				self.eService.activate({id: (self.route.queryParams as any).value.id, timestamp: (self.route.queryParams as any).value.stamp})
										.then(function successCallback(res){
											if(res == true)
												self.successActive = true;
											jQuery( ".full-layout" ).hide();
										}, function errorCallback(err){
					
										});
			} else {
				setTimeout(function(){
					jQuery( ".full-layout" ).hide();
				}, 2000);
			}
		}
		
		signUp(): void{
			let self = this;
			if(self.regUser.email && self.regUser.password && self.regUser.name && self.regUser.surname){
				let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						if(!re.test(String(self.regUser.email).toLowerCase())){
							self.signupError = "Email forması düzgün deyil";
							self.showSignupError = true;
							self.showUpEmailError = true;
						} else {
							self.signupError = "";
							self.showSignupError = false;
							self.showUpEmailError = false;
							self.eService.signUp({name: self.regUser.name, surname: self.regUser.surname, email: self.regUser.email, password: self.regUser.password, url: (window.location.origin + "/account?activate=true")})
										.then(function successCallback(res){
												console.log(res);
												if(res.id){
													self.successRes = true;
												} else if(res.error && res.error.message.indexOf("Duplicate entry") != -1){
														self.signupError = "Bu email artıq qeydiyyatdan keçib";
														self.showUpEmailError = true;
														self.showSignupError = true;
												} else {
														self.signupError = "Hal-hazırda qeydiyyatdan keçmək mümkün deyil";
														self.showSignupError = true;
												}
										},
													function errorCallback(err){
											console.log(err);
										});
						}
			} else {
				self.showSignupError = true;
				if(self.regUser.email){
					let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    			if(!re.test(String(self.regUser.email).toLowerCase())){
						self.signupError = "Email forması düzgün deyil";
						self.showUpEmailError = true;
					}
				}
			}
			
		}
		
		signIn(): void{
			let self = this;
			console.log(window.location.host);
				if(self.signUser.email && self.signUser.password){
						let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						if(!re.test(String(self.signUser.email).toLowerCase())){
							self.signinError = "Email forması düzgün deyil";
							self.showSigninError = true;
							self.showEmailError = true;
						} else {
							self.signinError = "";
							self.showSigninError = false;
							self.showEmailError = false;
							self.resendLink = false;
							self.resendSuccess = false;
							self.eService.signIn({email: self.signUser.email, password: self.signUser.password})
												.then(function successCallback(res){
														console.log(res);
														if(res[0] && res[0].id && res[0].validated){
															let time = 0;
															let domain = window.location.host;
															if(self.signUser.remember)
																time = 1000;
															else
																time = 0;
															self.cookieService.set('loggedin', 'true', time, "/", domain);
															self.cookieService.set('name', res[0].name, time, "/", domain);
															self.cookieService.set('surname', res[0].surname, time, "/", domain);
															self.cookieService.set('email', res[0].email, time, "/", domain);
															self.cookieService.set('userid', res[0].id, time, "/", domain);
															let cartTotal: any = 0, cartCount: any = 0;

															for(let cart in res[0].carts){
																cartCount += 	res[0].carts[cart].count;
																cartTotal += res[0].carts[cart].total;
															}

															self.cookieService.set('cartTotal', cartTotal, time, "/", domain);
															self.cookieService.set('cartCount', cartCount, time, "/", domain);

															window.location.href = '/';
														} else if(res[0] && res[0].id && !res[0].validated) {
															self.signinError = "Heasbınız aktiv deyil. Hesabınızı aktiv etmək üçün email ünvanınıza göndərdiyimiz keçidə daxil olun.";
															self.showSigninError = true;		
															self.resendLink = true;
														} else {
															self.signinError = "Email və ya şifrə doğru deyil";
															self.showSigninError = true;
														}
												},
															function errorCallback(err){
															console.log(err);
												});
						}			
			} else {
				self.showSigninError = true;
				if(self.signUser.email){
					let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    			if(!re.test(String(self.signUser.email).toLowerCase())){
						self.signinError = "Email forması düzgün deyil";
						self.showEmailError = true;
					}
				}
			}
		}
		
		resendNewLink(): void{
			let self = this;
			
			self.eService.resendLink({email: self.signUser.email, url: (window.location.origin + "/account?activate=true")})
									.then(function successCallback(res){
										console.log(res);
										self.signinError = "";
										self.showSigninError = false;
										self.resendLink = false;
										self.resendSuccess = true;
									}, function errorCallback(err){
										console.log(err);
									});
		}
		
		resendPass(): void{
			let self = this;
			if(self.lostUser.email){
				let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						if(!re.test(String(self.lostUser.email).toLowerCase())){
							self.lostError = "Email forması düzgün deyil";
							self.showLostError = true;
						} else {
							self.lostError = "";
							self.showLostError = false;
							self.showLostForm = true;
							self.lostSuccess = false;
							self.eService.lostPass({email: self.lostUser.email})
										.then(function successCallback(res){
												console.log(res);
												if(res == "not found"){
													self.lostError = "Bu email UNİMAX saytında qeydiyyatda yoxdu";
													self.showLostError = true;
												} else {
													self.lostSuccess = true;
													self.showLostForm = false;
												}
										},
													function errorCallback(err){
											console.log(err);
										});
						}
			} else {
				self.showLostError = true;
			}
		}

}
