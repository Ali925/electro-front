import { Component, ElementRef } from '@angular/core';
import { ElectroService } from '../electro.service';
import { OnInit } from '@angular/core';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-friendship',
  templateUrl: './friendship.component.html',
  styleUrls: ['./friendship.component.css']
})
export class FriendshipComponent implements OnInit {

		brends = [];
	models = [];
	blogs = [];
	
	constructor(private eService: ElectroService, private elementRef:ElementRef) {}
	
	ngOnInit(): void {
		let self = this;
		this.eService.getStatus()
			.then(function succesCallback(data){
				let body = data;
				self.brends = body.allFirms;
				self.blogs = body.allBlogTitles;
				self.models = body.allModels;
				for(let i=0;i<body.allTypes.length;i++){
					 for(let j in self.brends){
							if(self.brends[j].id == body.allTypes[i].firm_id){
								for(let k in self.brends[j].models){
									if(self.brends[j].models[k].id == body.allTypes[i].model_id){
												if(self.brends[j].models[k].types == undefined)
													self.brends[j].models[k].types = [];
												self.brends[j].models[k].types.push(body.allTypes[i]);
												break;
	}	
	}
	break;
	}
						}
				}		
				console.log(self.brends);
				var s = document.createElement("script");
				s.type = "text/javascript";
				s.src = "assets/js/electro.js";
				self.elementRef.nativeElement.appendChild(s);

				jQuery( ".full-layout" ).hide();
		},function errorCallback(error){
			console.log(error);
		});
	}
	
}
