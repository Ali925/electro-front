import { Component, ElementRef } from '@angular/core';
import { ElectroService } from '../electro.service';
import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-friendship-content',
  templateUrl: './friendship-content.component.html',
  styleUrls: ['./friendship-content.component.css']
})
export class FriendshipContentComponent implements OnInit {
	constructor(private route: ActivatedRoute) {}
	
	page = 1;
	
	ngOnInit() {
			let self = this;
				if((this.route.params as any).value && (this.route.params as any).value.id){
			 		self.page = (this.route.params as any).value.id;
				}
		}
}
