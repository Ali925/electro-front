import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class ElectroService {
	
	constructor(private http: HttpClient) {}
	
	api = window.location.protocol + "//admin.unimax.az";
	//api = "http://electro";
	
	getStatus(): Promise<any> {
		let self = this;
		return this.http.get(self.api + '/api/firstApp')
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	startHome(): Promise<any> {
		let self = this;
		return this.http.get(self.api + '/api/startHome')
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getFooter(): Promise<any> {
		let self = this;
		return this.http.get(self.api + '/api/getFooter')
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getFilter(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/filterProduct', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getProducts(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/getProducts', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getProduct(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/getProduct', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	signUp(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/signup', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	signIn(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/signin', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getCustomer(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/getuser', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getCart(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/getcart', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	removeCart(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/removecart', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	addCart(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/addcart', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	sendReview(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/addreview', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	sendComment(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/addcomment', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getBlogs(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/getblogs', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	getBlog(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/getblog', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	searchProduct(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/searchprod', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	searchBlog(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/searchblog', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	paginate(sendData, url): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(url, data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	activate(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/activate', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	resendLink(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/resendLink', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	lostPass(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/lostPass', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	sendMessage(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/sendMessage', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	subscribe(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/subscribe', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	addRemoveOneCart(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/addRemoveCart', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
	changeCartColor(sendData): Promise<any>{
		let self = this;
		let _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
		
		let data = JSON.stringify(sendData);
		
		return this.http.post(self.api + '/api/changeCartColor', data, _options)
   	 	.toPromise()
    	.then(response => response)
      .catch(error => error);
	}
	
}