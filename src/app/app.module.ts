import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ShareButtons } from '@ngx-share/core';
import { ShareButtonModule } from '@ngx-share/button';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { HomeHeaderComponent } from './home/home-header.component';
import { HomeContentComponent } from './home/home-content.component';
import { BrandsComponent } from './brands/brands.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { FooterComponent } from './footer/footer.component';
import { BlogComponent } from './blog/blog.component';
import { BlogContentComponent } from './blog/blog-content.component';
import { BlogDetailComponent } from './blog/blog-detail.component';
import { ShopComponent } from './shop/shop.component';
import { ShopDetailComponent } from './shop/shop-detail.component';
import { ShopContentComponent } from './shop/shop-content.component';
import { CartComponent } from './cart/cart.component';
import { CartContentComponent } from './cart/cart-content.component';
import { AboutComponent } from './about/about.component';
import { AboutContentComponent } from './about/about-content.component';
import { FriendshipComponent } from './friendship/friendship.component';
import { FriendshipContentComponent } from './friendship/friendship-content.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactsContentComponent } from './contacts/contacts-content.component';
import { AccountComponent } from './account/account.component';
import { AccountContentComponent } from './account/account-content.component';
import { PageNotFoundComponent } from './not-found/page-not-found.component';
import { SpinComponent } from './spin/spin.component';

import { ElectroService } from './electro.service';
import { CookieService } from 'ngx-cookie-service';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
	{
		path: 'blog',
		component: BlogComponent
	},
	{
		path: 'blog/:id',
		component: BlogComponent
	},
	{
		path: 'blog/:id/:cat',
		component: BlogDetailComponent
	},
	{
		path: 'blog.php/:id/:cat',
		redirectTo: 'blog/:id/:cat',
		pathMatch: 'full'
	},
	{
		path: 'shop',
		component: ShopComponent,
		data: {page: 1}
	},
	{
		path: 'shop/:id',
		component: ShopComponent,
		data: {page: 1}
	},
	{
		path: 'shop/:id/:num',
		component: ShopComponent,
		data: {page: 2}
	},
	{
		path: 'shop.php/:id/:num',
		redirectTo: 'shop/:id/:num',
		data: {page: 2},
		pathMatch: 'full'
	},
	{
		path: 'shop/:id/:num/:color',
		component: ShopComponent,
		data: {page: 2}
	},
	{
		path: 'shop.php/:id/:num/:color',
		redirectTo: 'shop/:id/:num/:color',
		data: {page: 2},
		pathMatch: 'full'
	},
	{
		path: 'account',
		component: AccountComponent
	},
	{
		path: 'friendship',
		component: FriendshipComponent
	},
	{
		path: 'friendship/:id',
		component: FriendshipComponent
	},
	{
		path: 'contacts',
		component: ContactsComponent
	},
	{
		path: 'cart',
		component: CartComponent
	},
	{
		path: 'about',
		component: AboutComponent
	},
	{
		path: '____spinner',
		component: SpinComponent
	},
  { path: '**', component: PageNotFoundComponent }
];



@NgModule({
  declarations: [
    AppComponent,
		PageNotFoundComponent,
		HomeComponent,
		HeaderComponent,
		HomeHeaderComponent,
		HomeContentComponent,
		BrandsComponent,
		FooterComponent,
		TopBarComponent,
		BlogComponent,
		BlogContentComponent,
		BlogDetailComponent,
		ShopComponent,
		ShopDetailComponent,
		ShopContentComponent,
		CartComponent,
		CartContentComponent,
		AboutComponent,
		AboutContentComponent,
		ContactsComponent,
		ContactsContentComponent,
		AccountComponent,
		AccountContentComponent,
		FriendshipComponent,
		FriendshipContentComponent,
		SpinComponent
  ],
  imports: [
		RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
		HttpClientModule,
		FormsModule,
		FontAwesomeModule,
		ShareButtonModule.forRoot()
  ],
  providers: [ElectroService,
						 CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
