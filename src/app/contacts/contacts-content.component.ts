import { Component } from '@angular/core';
import { ElectroService } from '../electro.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
	selector: 'app-contacts-content',
  templateUrl: './contacts-content.component.html',
  styleUrls: ['./contacts-content.component.css']
})
export class ContactsContentComponent {
	
	constructor(private eService: ElectroService, private cookieService: CookieService) {}
	
	isLogged = this.cookieService.get("loggedin");
	fullname = this.cookieService.get("name") + " " + this.cookieService.get("surname");
	name = this.cookieService.get("loggedin") == "true" ? this.fullname : "";
	email = this.cookieService.get("loggedin") == "true" ? this.cookieService.get("email") : "";
	title = "";
	message = "";
	showSuccess = false;
	
	showError = false;
	showEmailError = false;
	
	send(): void{
		let self = this;
		self.showError = false;
		self.showEmailError = false;
		if(self.name && self.email && self.message && self.title){
			let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						if(!re.test(String(self.email).toLowerCase())){
							self.showEmailError = true;
							self.showError = true;
						} else {
							self.eService.sendMessage({name: self.name, email: self.email, title: self.title, message: self.message})
													.then(function successCallback(res){
														console.log(res);
														self.showSuccess = true;
													}, function errorCallback(err){
														console.log(err);
													});
						}
		} else {
			self.showError = true;
			if(self.email){
				let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				if(!re.test(String(self.email).toLowerCase()))
					self.showEmailError = true;
			}
		}
	}
	
}
