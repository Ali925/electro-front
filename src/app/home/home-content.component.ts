import { Component, Input, IterableDiffers, IterableDiffer, OnInit, DoCheck, ElementRef } from '@angular/core';
import { ElectroService } from '../electro.service';
import { NgForOf } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';

import 'rxjs/Rx';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-home-content',
  templateUrl: './home-content.component.html',
  styleUrls: ['./home-content.component.css']
})
export class HomeContentComponent implements DoCheck {
	
		@Input() brends:any;
		@Input() models:any;
		@Input() blogs:any;
		@Input() types:any;
	
		differ:IterableDiffer<{}>;
	
		constructor(private route: ActivatedRoute, private differs: IterableDiffers, private elementRef:ElementRef, public eService: ElectroService, private cookieService: CookieService) {
			this.differ = differs.find([]).create(null);
		}
	
		isLogged = this.cookieService.get("loggedin");
		userid = this.cookieService.get("userid");
		selectedTabsModel = 1;
		lastProds = [];
		lastBlogs = [];
		selectedProds = [];
		discountProds = [];
		starsProds = [];
		smartphones = [];
		mobiles = [];
		tablets = [];
		access = [];
		selectedTab = [];
		carBanners = [];
		centerBanner: any = {};
	
		selectTabModel(id): void {
			this.selectedTabsModel = id;
			if(id==1)
				this.selectedTab = this.smartphones;
			else if(id==2)
				this.selectedTab = this.mobiles;
			else if(id==4)
				this.selectedTab = this.tablets;
			else if(id==5)
				this.selectedTab = this.access;
		}
	
		ngDoCheck() {
			let self = this;
			const change = this.differ.diff(this.models);
			if(change != null){
				self.eService.startHome()
										.then(function successCallback(res){
												console.log(res);
											self.lastProds = res.lastProducts;
											self.selectedProds = res.mostSelected;
											self.discountProds = res.discountProducts;
											self.starsProds = res.starProducts;
											self.smartphones = res.smartphones;
											self.mobiles = res.mobiles;
											self.tablets = res.tablets;
											self.access = res.access;
											self.lastBlogs = res.lastblogs;
											for(let b in res.banners){
												if(res.banners[b].type == '1')
													self.carBanners.push(res.banners[b]);
												if(res.banners[b].type == '2')
													self.centerBanner = res.banners[b];
											}
					
											for(let b in self.lastBlogs){
												self.lastBlogs[b].created_at = self.lastBlogs[b].created_at.substr(0,10);
											}
					
											for(let r in self.lastProds){
												for(let m in self.models){
													if(self.lastProds[r].model_id == self.models[m].id){
														self.lastProds[r].model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.lastProds[r].firm_id == self.brends[m].id){
														self.lastProds[r].firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.lastProds[r].type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.lastProds[r].type_name = names.join(" ");
														break;
													}
												}
												self.lastProds[r].images = JSON.parse(self.lastProds[r].images);
											}
					
											for(let r in self.selectedProds){
												for(let m in self.models){
													if(self.selectedProds[r].products.model_id == self.models[m].id){
														self.selectedProds[r].products.model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.selectedProds[r].products.firm_id == self.brends[m].id){
														self.selectedProds[r].products.firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.selectedProds[r].products.type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.selectedProds[r].products.type_name = names.join(" ");
														break;
													}
												}
												self.selectedProds[r].products.images = JSON.parse(self.selectedProds[r].products.images);
											}
							
											for(let r in self.discountProds){
												for(let m in self.models){
													if(self.discountProds[r].model_id == self.models[m].id){
														self.discountProds[r].model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.discountProds[r].firm_id == self.brends[m].id){
														self.discountProds[r].firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.discountProds[r].type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.discountProds[r].type_name = names.join(" ");
														break;
													}
												}
												self.discountProds[r].images = JSON.parse(self.discountProds[r].images);
											}
					
											for(let r in self.starsProds){
												for(let m in self.models){
													if(self.starsProds[r].products.model_id == self.models[m].id){
														self.starsProds[r].products.model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.starsProds[r].products.firm_id == self.brends[m].id){
														self.starsProds[r].products.firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.starsProds[r].products.type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.starsProds[r].products.type_name = names.join(" ");
														break;
													}
												}
												self.starsProds[r].products.images = JSON.parse(self.starsProds[r].products.images);
											}
					
											for(let r in self.smartphones){
												for(let m in self.models){
													if(self.smartphones[r].model_id == self.models[m].id){
														self.smartphones[r].model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.smartphones[r].firm_id == self.brends[m].id){
														self.smartphones[r].firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.smartphones[r].type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.smartphones[r].type_name = names.join(" ");
														break;
													}
												}
												self.smartphones[r].images = JSON.parse(self.smartphones[r].images);
											}
					
											for(let r in self.mobiles){
												for(let m in self.models){
													if(self.mobiles[r].model_id == self.models[m].id){
														self.mobiles[r].model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.mobiles[r].firm_id == self.brends[m].id){
														self.mobiles[r].firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.mobiles[r].type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.mobiles[r].type_name = names.join(" ");
														break;
													}
												}
												self.mobiles[r].images = JSON.parse(self.mobiles[r].images);
											}
					
											for(let r in self.tablets){
												for(let m in self.models){
													if(self.tablets[r].model_id == self.models[m].id){
														self.tablets[r].model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.tablets[r].firm_id == self.brends[m].id){
														self.tablets[r].firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.tablets[r].type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.tablets[r].type_name = names.join(" ");
														break;
													}
												}
												self.tablets[r].images = JSON.parse(self.tablets[r].images);
											}
					
											for(let r in self.access){
												for(let m in self.models){
													if(self.access[r].model_id == self.models[m].id){
														self.access[r].model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(self.access[r].firm_id == self.brends[m].id){
														self.access[r].firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(self.access[r].type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														self.access[r].type_name = names.join(" ");
														break;
													}
												}
												self.access[r].images = JSON.parse(self.access[r].images);
											}
										self.selectedTab = self.smartphones;
										var s = document.createElement("script");
										s.type = "text/javascript";
										s.src = "assets/js/electro.js";
										self.elementRef.nativeElement.appendChild(s);
					
										jQuery( ".full-layout" ).hide();
					
								}, function errorCallback(err){
										console.log(err);
				});
			}
		}
	
	addToCart(product): void{
			let self = this;
			if(this.isLogged == 'true'){
			let total = 0;
				if(product.is_discount)
					total = product.new_price;
				else
					total = product.price;
				self.eService.addCart({userid: self.userid, productid: product.id, count: 1, total: total})
					.then(function successCallback(res){
						console.log(res);
						if(res.id){
							window.location.href = "/cart";
						}
					}, function errorCallback(err){
						console.log(err);
					});
			} else {
				window.location.href = "/account";
			}
		}
		
}
