import { Component, Input, OnInit, IterableDiffers, IterableDiffer, DoCheck } from '@angular/core';
import { NgForOf } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { ElectroService } from '../electro.service';

declare var jQuery:any;

@Component({
	selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.css']
})
export class HomeHeaderComponent implements DoCheck {

		@Input() brends:any;
		@Input() models:any;
		@Input() blogs:any;
		@Input() types:any;
	
		differ:IterableDiffer<{}>;
	
		constructor(private cookieService: CookieService, private eService: ElectroService, private differs: IterableDiffers) {
			this.differ = differs.find([]).create(null);
		}
	
		isLogged = this.cookieService.get("loggedin");
		cartTotal = 0;
		cartCount = 0;
		cartProds = [];
		searchString = '';
		searchId = 0;
	
		ngDoCheck(): void{
			let self = this;
			const change = this.differ.diff(this.models);
			if(change != null){
				if(self.isLogged == 'true'){
					self.eService.getCustomer({email: self.cookieService.get("email")})
											.then(function successCallback(res){
												console.log(res);

												for(let r in res.carts){
													self.cartCount += res.carts[r].count;
													self.cartTotal += res.carts[r].total;

													for(let m in self.models){
														if(res.carts[r].products.model_id == self.models[m].id){
															res.carts[r].products.model_name = self.models[m].name;
															break;
														}
													}
													for(let m in self.brends){
														if(res.carts[r].products.firm_id == self.brends[m].id){
															res.carts[r].products.firm_name = self.brends[m].name;
															break;
														}
													}
													for(let m in self.types){
														if(res.carts[r].products.type_id == self.types[m].id){
															let names = self.types[m].name.split(" ");
															for(let i in names){
																if(names[i].indexOf("seriya")!=-1){
																	names.splice(i, 1);
																	break;
																}
															}
															res.carts[r].products.type_name = names.join(" ");
															break;
														}
													}
													res.carts[r].products.images = JSON.parse(res.carts[r].products.images);
												}

												self.cartProds = res.carts;
					}, function errorCallback(err){
						console.log(err);
					});
				}
			}	
		}
		
		removeCart(id): void{
			let self = this;
			this.eService.removeCart({itemid: id})
										.then(function successCallback(res){
													console.log(res);

													window.location.reload();
											},
														function errorCallback(err){
													console.log(err);
											});
		}
		
		search(): void{
			let self = this;
			console.log(self.searchString, self.searchId);
			if(self.searchString){
				window.location.href = window.location.origin + "/shop/search?id=" + self.searchId + "&string=" + self.searchString;
			}
		}
		
		showMenu(): void{
			jQuery("li.yamm-tfw.menu-item").css('visibility', 'visible');
			jQuery("li.yamm-tfw.menu-item").css('opacity', '1');
			jQuery(".header-v1 .vertical-menu").css('z-index', '1000');
			jQuery(".header-v1 .vertical-menu").css('background-color', 'white');
			
			jQuery("li.list-group-item").removeClass("hide-other");
		}

		hideMenu(): void{
			jQuery("li.yamm-tfw.menu-item").css('visibility', 'collapse');
			jQuery("li.yamm-tfw.menu-item").css('opacity', '0');
			jQuery(".header-v1 .vertical-menu").css('z-index', '1');
			jQuery(".header-v1 .vertical-menu").css('background-color', 'transparent');
			
			jQuery("li.list-group-item").addClass("hide-other");
		}
	
}
