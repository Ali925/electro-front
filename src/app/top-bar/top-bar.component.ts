import { Component } from '@angular/core';
import { ElectroService } from '../electro.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
	selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent {
	
	constructor(private eService: ElectroService, private cookieService: CookieService) {}
	
	isLogged = this.cookieService.get("loggedin");
	username = this.cookieService.get("name");
	
	logout(): void{
		let domain = window.location.host;
		this.cookieService.deleteAll('/', domain);
		this.isLogged = 'false';
		this.username = '';
		window.location.reload();
	}
}
