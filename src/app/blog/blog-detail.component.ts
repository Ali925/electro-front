import { Component, ElementRef } from '@angular/core';
import { ElectroService } from '../electro.service';
import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import	{DomSanitizer} from '@angular/platform-browser';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit  {
	brends = [];
	models = [];
	blogs = [];
	
	constructor(public eService: ElectroService, private elementRef:ElementRef, private route: ActivatedRoute, private cookieService: CookieService, private sanitizer: DomSanitizer) {
		
	}
	
	isLogged = this.cookieService.get("loggedin");
	userid = this.cookieService.get("userid");
	username = this.cookieService.get("name");
	surname = this.cookieService.get("surname");
	email = this.cookieService.get("email");
	
	blogID = null;
	pageID = null;
	blogTitles = [];
	currentBlog: any = {};
	blogTags = [];
	blogLast = [];
	comments = [];
	formName = "";
	formEmail = "";
	formComment = "";
	searchString = "";
	
	currentURL = "";
	
	ngOnInit(): void {
		let self = this;
		
		let pathname = window.location.pathname.split("/");
		this.currentURL = "http://unimax.az/blog.php/" + pathname[2] + "/" + pathname[3];
		
		this.pageID = (this.route.params as any).value.id;
		this.blogID = (this.route.params as any).value.cat;
		this.eService.getStatus()
			.then(function succesCallback(data){
				let body = data;
				self.brends = body.allFirms;
				self.blogs = body.allBlogTitles;
				self.models = body.allModels;
				for(let i=0;i<body.allTypes.length;i++){
					 for(let j in self.brends){
							if(self.brends[j].id == body.allTypes[i].firm_id){
								for(let k in self.brends[j].models){
									if(self.brends[j].models[k].id == body.allTypes[i].model_id){
												if(self.brends[j].models[k].types == undefined)
													self.brends[j].models[k].types = [];
												self.brends[j].models[k].types.push(body.allTypes[i]);
												break;
	}	
	}
	break;
	}
						}
				}		
				console.log(self.brends);
				self.eService.getBlog({blogid: self.blogID})
											.then(function successCallback(res){
								console.log(res);
								self.blogTitles = res.titles;
								self.blogTags = res.tags;
								self.blogLast = res.last;
								self.currentBlog = res.post;
								self.comments = res.comments;
								self.currentBlog.text = self.sanitizer.bypassSecurityTrustHtml(self.currentBlog.text);
								self.currentBlog.created_at = self.currentBlog.created_at.substr(0, 10);
					
								for(let c in self.comments){
									self.comments[c].created_at = self.comments[c].created_at.substr(0, 10);
								}
					
							let s = document.createElement("script");
							s.type = "text/javascript";
							s.src = "assets/js/electro.js";
							self.elementRef.nativeElement.appendChild(s);
						
							jQuery( ".full-layout" ).hide();
				}, function errorCallback(err){
					
				});
				
		},function errorCallback(error){
			console.log(error);
		});
	}
	
	sendComment(): void{
		let self = this;
		if(self.isLogged == 'true' && self.formComment){
			self.eService.sendComment({userid: self.userid, blogid: self.blogID, comment: self.formComment})
									.then(function successCallback(res){
										console.log(res);
										if(res.id){
											window.location.href = window.location.href + "#comments";
											window.location.reload();
										}
									}, function errorCallback(err){
										console.log(err);
									});
		} else if(self.formComment && self.formName && self.formEmail){
				self.eService.sendComment({blogid: self.blogID, comment: self.formComment, username: self.formName, email: self.formEmail})
									.then(function successCallback(res){
										console.log(res);
									if(res.id){
										window.location.href = window.location.href + "#comments";
										window.location.reload();
									}
									}, function errorCallback(err){
										console.log(err);
									});
		}
		
	}
	
	search(): void{
		let self = this;
		
		if(self.searchString){
			window.location.href = window.location.origin + "/blog/search?string=" + self.searchString;
		}
	}

}
