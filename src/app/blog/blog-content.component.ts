import { Component, ElementRef } from '@angular/core';
import { ElectroService } from '../electro.service';
import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-blog-content',
  templateUrl: './blog-content.component.html',
  styleUrls: ['./blog-content.component.css']
})
export class BlogContentComponent implements OnInit {
	constructor(private route: ActivatedRoute, private eService: ElectroService) {}
	
	pageID = 1;
	blogTitles = [];
	blogPosts = [];
	blogTags = [];
	blogLast = [];
	currentTitle = '';
	searchString = "";
	currentString = "";
	currentTag = "";
	currentTagName = "";
	
	lastPage = null;
	currentPage = null;
	prevPageUrl: any = '';
	nextPageUrl = '';
	lastPageUrl = '';
	
	ngOnInit() {
			let self = this;
				if((this.route.params as any).value && (this.route.params as any).value.id == 'search'){
					self.currentString = (self.route.queryParams as any).value.string;
					self.pageID = 0;
					this.eService.searchBlog({string: self.currentString})
												.then(function successCallback(res){
													console.log(res);
													self.blogTitles = res.titles;
													self.blogPosts = res.posts;
													self.blogTags = res.tags;
													self.blogLast = res.last;
													for(let b in self.blogTitles){
														if(self.blogTitles[b].id == self.pageID){
															self.currentTitle = self.blogTitles[b].name;
															break;
														}
													}

										for(let b in self.blogPosts){
											self.blogPosts[b].created_at = self.blogPosts[b].created_at.substr(0, 10);
										}
						jQuery( ".full-layout" ).hide();
					}, function errorCallback(err){
						console.log(err);
					});
				} else if((this.route.params as any).value && (this.route.params as any).value.id == 'tags'){
					self.currentTag = (self.route.queryParams as any).value.id;
					self.pageID = 0;
					
					this.eService.searchBlog({tagID: self.currentTag})
												.then(function successCallback(res){
													console.log(res);
													self.blogTitles = res.titles;
													self.blogPosts = res.posts;
													self.blogTags = res.tags;
													self.blogLast = res.last;
													for(let b in self.blogTitles){
														if(self.blogTitles[b].id == self.pageID){
															self.currentTitle = self.blogTitles[b].name;
															break;
														}
													}

										for(let b in self.blogPosts){
											self.blogPosts[b].created_at = self.blogPosts[b].created_at.substr(0, 10);
										}
						
										for(let t in self.blogTags){
											if(self.blogTags[t].id == self.currentTag){
												self.currentTagName = self.blogTags[t].name;
												break;
											}
										}
						jQuery( ".full-layout" ).hide();
					}, function errorCallback(err){
						console.log(err);
					});
				} else if((this.route.params as any).value && (this.route.params as any).value.id){
			 		self.pageID = (this.route.params as any).value.id;
				
		
					this.eService.getBlogs({pageid: self.pageID})
												.then(function successCallback(res){
													console.log(res);
										self.blogTitles = res.titles;
										self.blogPosts = res.posts.data;
										self.blogTags = res.tags;
										self.blogLast = res.last;
						
										self.lastPage = res.posts.last_page;
										self.currentPage = res.posts.current_page;
										self.prevPageUrl = res.posts.prev_page_url;
										self.nextPageUrl = res.posts.next_page_url;
										self.lastPageUrl = res.posts.last_page_url;
						
										for(let b in self.blogTitles){
											if(self.blogTitles[b].id == self.pageID){
												self.currentTitle = self.blogTitles[b].name;
												break;
											}
										}

										for(let b in self.blogPosts){
											self.blogPosts[b].created_at = self.blogPosts[b].created_at.substr(0, 10);
										}
						jQuery( ".full-layout" ).hide();
					}, function errorCallback(err){
						console.log(err);
					});
			 }
		}
	
	search(): void{
		let self = this;
		
		if(self.searchString){
			window.location.href = window.location.origin + "/blog/search?string=" + self.searchString;
		}
	}

	paginate(url): void{
		let self = this;
		
		this.eService.paginate({pageid: self.pageID}, url)
												.then(function successCallback(res){
													console.log(res);
													
										self.blogPosts = res.posts.data;
						
										self.lastPage = res.posts.last_page;
										self.currentPage = res.posts.current_page;
										self.prevPageUrl = res.posts.prev_page_url;
										self.nextPageUrl = res.posts.next_page_url;
										self.lastPageUrl = res.posts.last_page_url;
						

										for(let b in self.blogPosts){
											self.blogPosts[b].created_at = self.blogPosts[b].created_at.substr(0, 10);
										}
					}, function errorCallback(err){
						console.log(err);
					});
	}
	
}
