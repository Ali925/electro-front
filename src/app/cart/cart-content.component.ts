import { Component, Input, DoCheck, ElementRef, IterableDiffers, IterableDiffer } from '@angular/core';
import { NgForOf, Location } from '@angular/common';
import { ElectroService } from '../electro.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/Rx';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-cart-content',
  templateUrl: './cart-content.component.html',
  styleUrls: ['./cart-content.component.css']
})
export class CartContentComponent implements DoCheck{
	
		@Input() brends:any;
		@Input() models:any;
	
	differ:IterableDiffer<{}>;
	
	constructor(private eService: ElectroService, private cookieService: CookieService, private differs: IterableDiffers, private location: Location) {
		this.differ = differs.find([]).create(null);
	}
	
	userid = this.cookieService.get("userid");
	products = [];
	colors = [];
	cartTotal = 0;
	cartCount = 0;
	
	ngDoCheck(): void{
		let self = this;
		const change = this.differ.diff(this.models);
			if(change != null){
		this.eService.getCart({userid: self.userid})
										.then(function successCallback(res){
												console.log(res);
												self.products = res.cart;
												self.colors = res.colors;
												self.cartCount = self.products.length;
												for(let s in self.products){
													self.cartTotal += self.products[s].total;
													self.products[s].products.images = JSON.parse(self.products[s].products.images);
													for(let b in self.brends){
														console.log(self.products[s].products.firm_id,self.brends[b].id);
														if(self.products[s].products.firm_id==self.brends[b].id){
															self.products[s].products.firm_name = self.brends[b].name;
															break;
														}
													}
													
													if(self.products[s].color_id != null){
														for(let c in self.colors[s].colors){
															if(self.colors[s].colors[c].id == self.products[s].color_id){
																self.products[s].color_code = self.colors[s].colors[c].code.substr(1);
																break;
															}
														}
														for(let i in self.products[s].products.images){
															if(self.products[s].products.images[i].indexOf(self.products[s].color_code) != -1){
																self.products[s].image_url = self.products[s].products.images[i];
																break;
															}
														}
														
													}
												}
											jQuery( ".full-layout" ).hide();
										},
													function errorCallback(err){
												console.log(err);
										});
		}
	}
	
	remove(id): void{
		let self = this;
		this.eService.removeCart({itemid: id})
									.then(function successCallback(res){
												console.log(res);
												
												window.location.href = "/cart";
										},
													function errorCallback(err){
												console.log(err);
										});
	}
	
	addOne(product): void{
		let self = this;
		self.eService.addRemoveOneCart({type: "add", id: product.id})
									.then(function successCallback(res){
											console.log(res);
											window.location.reload();
									}, function errorCallback(err){
											console.log(err);
									});
	}

	removeOne(product): void{
		let self = this;
		if(product.count > 1){
			self.eService.addRemoveOneCart({type: "remove", id: product.id})
									.then(function successCallback(res){
											console.log(res);
											window.location.reload();
									}, function errorCallback(err){
											console.log(err);
									});
		}
	}
	
	changeColor(product): void{
		let self = this;
		self.eService.changeCartColor({color_id: product.color_id, id: product.id})
									.then(function successCallback(res){
											console.log(res);
											window.location.reload();
									}, function errorCallback(err){
											console.log(err);
									});
	}

}
