import { Component } from '@angular/core';
import { ElectroService } from '../electro.service';
import { OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
	selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
		
	constructor(private eService: ElectroService, private cookieService: CookieService) {}
	
	blogs = [];
	models = [];
	
	isLogged = this.cookieService.get("loggedin");
	userid = this.cookieService.get("userid");
	
	email = "";
	subscribeSuccess = false;
	subscribeFail = false;
	
	ngOnInit(): void{
		let self = this;
		self.eService.getFooter()
								.then(function successCallback(res){
								console.log(res);
								self.blogs = res.allBlogTitles;
								self.models = res.allModels;
	}, function errorCallback(err){
		console.log(err);
	});
	}
	
	subscribe(): void{
		let self = this;
		let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(self.email != "" && re.test(String(self.email).toLowerCase())){
			self.eService.subscribe({email: self.email})
									.then(function successCallback(res){
										console.log(res);
										if(res.email){
											self.subscribeSuccess = true;
										} else if(res.error && res.error.message.indexOf("Duplicate entry") != -1){
											self.subscribeFail = true;
										}
									}, function errorCallback(err){
										console.log(err);
									});
		}
	}
}
