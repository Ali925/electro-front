import { Component, Input, OnInit, IterableDiffers, IterableDiffer, DoCheck } from '@angular/core';
import { NgForOf } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { ElectroService } from '../electro.service';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements DoCheck{
		@Input() brends:any;
		@Input() models:any;
		@Input() blogs:any;
	
		differ:IterableDiffer<{}>;
	
		constructor(private cookieService: CookieService, private eService: ElectroService, private differs: IterableDiffers) {
			this.differ = differs.find([]).create(null);
		}
	
		isLogged = this.cookieService.get("loggedin");
		cartTotal = 0;
		cartCount = 0;
		cartProds = [];
		searchString = '';
		searchId = 0;
	
		ngDoCheck(): void{
			let self = this;
			const change = this.differ.diff(this.models);
			if(change != null){
				if(self.isLogged == 'true'){
					self.eService.getCustomer({email: self.cookieService.get("email")})
											.then(function successCallback(res){
												console.log(res);

												for(let r in res.carts){
													self.cartCount += res.carts[r].count;
													self.cartTotal += res.carts[r].total;

													for(let m in self.models){
														if(res.carts[r].products.model_id == self.models[m].id){
															res.carts[r].products.model_name = self.models[m].name;
															break;
														}
													}
													for(let m in self.brends){
														if(res.carts[r].products.firm_id == self.brends[m].id){
															res.carts[r].products.firm_name = self.brends[m].name;
															break;
														}
													}

													res.carts[r].products.images = JSON.parse(res.carts[r].products.images);
												}

												self.cartProds = res.carts;
					}, function errorCallback(err){
						console.log(err);
					});
				}
			}	
		}
		
		removeCart(id): void{
			let self = this;
			this.eService.removeCart({itemid: id})
										.then(function successCallback(res){
													console.log(res);

													window.location.reload();
											},
														function errorCallback(err){
													console.log(err);
											});
		}
		
		search(): void{
			let self = this;
			console.log(self.searchString, self.searchId);
			if(self.searchString){
				window.location.href = window.location.origin + "/shop/search?id=" + self.searchId + "&string=" + self.searchString;
			}
		}

}
