import { Component, OnInit, Input, IterableDiffers, IterableDiffer, DoCheck, ElementRef } from '@angular/core';
import { NgForOf } from '@angular/common';
import { ElectroService } from '../electro.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/Rx';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-shop-content',
  templateUrl: './shop-content.component.html',
  styleUrls: ['./shop-content.component.css']
})
export class ShopContentComponent implements OnInit, DoCheck {
	
		@Input() brends:any;
		@Input() models:any;
		@Input() types:any;
	
		products = [];
		colors = [];
		lastProducts = [];
	
		differ:IterableDiffer<{}>;
	
		constructor(private route: ActivatedRoute, private differs: IterableDiffers, private elementRef:ElementRef, public eService: ElectroService, private cookieService: CookieService) {
			this.differ = differs.find([]).create(null);
		}

		currentCategory: any = 0;
		currentString: any = '';
		currentModel = null;
		currentBrend = null;
		currentModelTitle = '';
		currentCategoryTitle = 'Bütün məhsullar';
		currentBrends = [];
		currentBrendsO: any = {};
		modelsCount = 0;
		maxPrice = 0;
		minPrice = 0;
		
		lastPage = null;
		currentPage = null;
		prevPageUrl: any = '';
		nextPageUrl = '';
		lastPageUrl = '';
		fromProd = null;
		toProd = null;
		totalProd = null;
		banner: any = {};
		
	
		isLogged = this.cookieService.get("loggedin");
		userid = this.cookieService.get("userid");
		
		showBy = [
			{
				value: 15,
				show: "Göstər: 15"
			},
			{
				value: 24,
				show: "Göstər: 24"
			},
			{
				value: 36,
				show: "Göstər: 36"
			},
		];
	
		sortBy = [
			{
				value: "id:DESC",
				title: "Tarix ilə"
			},
			{
				value: "price:ASC",
				title: "Əvvəlcə ucuz"
			},
			{
				value: "price:DESC",
				title: "Əvvəlcə baha"
			}
		];
	
		selectedSort = this.sortBy[0];
	
		selectedShow = this.showBy[0];
	
		ngOnInit() {
			let self = this;
			console.log(this.route);
				if((this.route.params as any).value.id == "search"){
					self.currentCategory = (self.route.queryParams as any).value.id;
					self.currentString = (self.route.queryParams as any).value.string;
				}
				else if((this.route.params as any).value.id){
			 		let c = (self.route.params as any).value.id.split('@');
					let b = (self.route.params as any).value.id.split('*');
					if((self.route.params as any).value.id.includes('@')){
						self.currentCategory = c[0];
						if(c.length>1)
							self.currentModel = c[1];
						else
							self.currentModel = null;
					} else if((self.route.params as any).value.id.includes('*')) {
						self.currentCategory = b[0];
						if(b.length>1)
							self.currentBrend = b[1];
						else
							self.currentBrend = null;
					} else{
						self.currentCategory = c[0];
					}
					
					console.log('lehlulu', c, self.currentCategory, self.currentModel);
				}
			
				if((self.route.queryParams as any).value.sort){
					for(let s in self.sortBy){
						if(self.sortBy[s].value == (self.route.queryParams as any).value.sort){
							self.selectedSort = self.sortBy[s];
							break;
						}
					}
				}
			
				if((self.route.queryParams as any).value.show){
					for(let s in self.showBy){
						if(self.showBy[s].value == (self.route.queryParams as any).value.show){
							self.selectedShow = self.showBy[s];
							break;
						}
					}
				}
		}
	
		ngDoCheck() {
			let self = this;
			const change = this.differ.diff(this.models);
			if(change != null){
				for(let i in this.models){
						if(this.currentCategory == this.models[i].id){
							this.currentCategoryTitle = this.models[i].name;
							break;
						}
					}
				for(let i in this.types){
					if(this.currentModel == this.types[i].id){
						this.currentModelTitle = this.types[i].name;
						break;
					}
				}
				self.modelsCount = self.models[0].models_count;
				if(self.currentString){
					self.eService.searchProduct({model_id: self.currentCategory, string: self.currentString, sort: self.selectedSort.value}).then(function successCallback(res){
					console.log(res);
					let data = res.allProducts;
					self.lastProducts = res.lastProducts;
					self.maxPrice = Math.max(res.maxPrice, res.maxNewPrice);
					self.banner = res.banner[0];
					if(res.minNewPrice != null)
						self.minPrice = Math.min(res.minPrice, res.minNewPrice);
					else 
						self.minPrice = res.minPrice;
					
					for(let r in data){
						for(let m in self.models){
							if(data[r].model_id == self.models[m].id){
								data[r].model_name = self.models[m].name;
								break;
							}
						}
						for(let m in self.brends){
							if(data[r].firm_id == self.brends[m].id){
								data[r].firm_name = self.brends[m].name;
								if(self.currentBrendsO[self.brends[m].id]){
									self.currentBrendsO[self.brends[m].id].count++; 
								} else {
									self.currentBrendsO[self.brends[m].id] = {
													name: self.brends[m].name,
													count: 1,
													id: self.brends[m].id
												};
								}
								break;
							}
						}
						for(let m in self.types){
							if(data[r].type_id == self.types[m].id){
								let names = self.types[m].name.split(" ");
								for(let i in names){
									if(names[i].indexOf("seriya")!=-1){
										names.splice(i, 1);
										break;
									}
								}
								data[r].type_name = names.join(" ");
								break;
							}
						}
						data[r].images = JSON.parse(data[r].images);
						for(let j in data[r].colors){
							let key = false;
							for(let m in self.colors){
								if(self.colors[m].id == data[r].colors[j].id){
									self.colors[m].count++;
									key = true;
									break;
								}
							}
							 if(!key) {
								 self.colors.push(data[r].colors[j]);
									self.colors[self.colors.length-1].count = 1;
							}
						}
						for(let m in data[r].reviews){
								if(data[r].reviews[m].stars != null && data[r].reviewsCount == undefined){
										data[r].reviewsCount = 1;
										data[r].reviewsA = data[r].reviews[m].stars;
									}
									else if(data[r].reviews[m].stars != null){
										data[r].reviewsCount++;
										data[r].reviewsA += data[r].reviews[m].stars;
									}
								}
								
								if(data[r].reviewsCount != undefined){
									data[r].reviewsA = parseInt(data[r].reviewsA)/data[r].reviewsCount;
									data[r].reviewsAw = data[r].reviewsA/5*100 + "%";
								} else {
									data[r].reviewsA = 0;
									data[r].reviewsAw = 0 + "%";
								}
						
					}
					
					for(let r in self.lastProducts){
						for(let m in self.models){
							if(self.lastProducts[r].model_id == self.models[m].id){
								self.lastProducts[r].model_name = self.models[m].name;
								break;
							}
						}
						for(let m in self.brends){
							if(self.lastProducts[r].firm_id == self.brends[m].id){
								self.lastProducts[r].firm_name = self.brends[m].name;
								break;
							}
						}
						for(let m in self.types){
							if(self.lastProducts[r].type_id == self.types[m].id){
								let names = self.types[m].name.split(" ");
								for(let i in names){
									if(names[i].indexOf("seriya")!=-1){
										names.splice(i, 1);
										break;
									}
								}
								self.lastProducts[r].type_name = names.join(" ");
								break;
							}
						}
						self.lastProducts[r].images = JSON.parse(self.lastProducts[r].images);
						for(let j in self.lastProducts[r].colors){
							let key = false;
							for(let m in self.colors){
								if(self.colors[m].id == self.lastProducts[r].colors[j].id){
									self.colors[m].count++;
									key = true;
									break;
								}
							}
							 if(!key) {
								 self.colors.push(self.lastProducts[r].colors[j]);
									self.colors[self.colors.length-1].count = 1;
							}
						}
					}
					
					for(let i in self.currentBrendsO){
						self.currentBrends.push(self.currentBrendsO[i]);
					}
					self.products = data;
					
					var s = document.createElement("script");
					s.type = "text/javascript";
					s.src = "assets/js/electro.js";
					self.elementRef.nativeElement.appendChild(s);

						jQuery( ".price_slider" ).slider({
							animate: "fast",
							range: true,
							step: 1,
							min: self.minPrice,
							max: self.maxPrice,
							values: [self.minPrice, self.maxPrice],
							change: function( event, ui ) {
								console.log(event, ui);
								self.minPrice = ui.values[0];
								self.maxPrice = ui.values[1];
							}
						});
						jQuery( ".full-layout" ).hide();
					}, function errorCallback(error){
						console.log(error);
					});
				} else {
				self.eService.getProducts({model_id: self.currentCategory, type_id: self.currentModel, brend_id: self.currentBrend, sort: self.selectedSort.value, show: self.selectedShow.value}).then(function successCallback(res){
					console.log(res);
					let data = res.allProducts.data;
					self.lastProducts = res.lastProducts;
					self.banner = res.banner[0];
					self.maxPrice = Math.max(res.maxPrice, res.maxNewPrice);
					if(res.minNewPrice != null)
						self.minPrice = Math.min(res.minPrice, res.minNewPrice);
					else 
						self.minPrice = res.minPrice;
					
					self.lastPage = res.allProducts.last_page;
					self.currentPage = res.allProducts.current_page;
					self.prevPageUrl = res.allProducts.prev_page_url;
					self.nextPageUrl = res.allProducts.next_page_url;
					self.lastPageUrl = res.allProducts.last_page_url;
					self.fromProd = res.allProducts.from;
					self.toProd = res.allProducts.to;
					self.totalProd = res.allProducts.total;
					
					for(let r in data){
						for(let m in self.models){
							if(data[r].model_id == self.models[m].id){
								data[r].model_name = self.models[m].name;
								break;
							}
						}
						for(let m in self.brends){
							if(data[r].firm_id == self.brends[m].id){
								data[r].firm_name = self.brends[m].name;
								if(self.currentBrendsO[self.brends[m].id]){
									self.currentBrendsO[self.brends[m].id].count++; 
								} else {
									self.currentBrendsO[self.brends[m].id] = {
													name: self.brends[m].name,
													count: 1,
													id: self.brends[m].id
												};
								}
								break;
							}
						}
						for(let m in self.types){
							if(data[r].type_id == self.types[m].id){
								let names = self.types[m].name.split(" ");
								for(let i in names){
									if(names[i].indexOf("seriya")!=-1){
										names.splice(i, 1);
										break;
									}
								}
								data[r].type_name = names.join(" ");
								break;
							}
						}
						data[r].images = JSON.parse(data[r].images);
						for(let j in data[r].colors){
							let key = false;
							for(let m in self.colors){
								if(self.colors[m].id == data[r].colors[j].id){
									self.colors[m].count++;
									key = true;
									break;
								}
							}
							 if(!key) {
								 self.colors.push(data[r].colors[j]);
									self.colors[self.colors.length-1].count = 1;
							}
						}
						for(let m in data[r].reviews){
								if(data[r].reviews[m].stars != null && data[r].reviewsCount == undefined){
										data[r].reviewsCount = 1;
										data[r].reviewsA = data[r].reviews[m].stars;
									}
									else if(data[r].reviews[m].stars != null){
										data[r].reviewsCount++;
										data[r].reviewsA += data[r].reviews[m].stars;
									}
								}
								
								if(data[r].reviewsCount != undefined){
									data[r].reviewsA = parseInt(data[r].reviewsA)/data[r].reviewsCount;
									data[r].reviewsAw = data[r].reviewsA/5*100 + "%";
								} else {
									data[r].reviewsA = 0;
									data[r].reviewsAw = 0 + "%";
								}
					}
					
					for(let r in self.lastProducts){
						for(let m in self.models){
							if(self.lastProducts[r].model_id == self.models[m].id){
								self.lastProducts[r].model_name = self.models[m].name;
								break;
							}
						}
						for(let m in self.brends){
							if(self.lastProducts[r].firm_id == self.brends[m].id){
								self.lastProducts[r].firm_name = self.brends[m].name;
								break;
							}
						}
						for(let m in self.types){
							if(self.lastProducts[r].type_id == self.types[m].id){
								let names = self.types[m].name.split(" ");
								for(let i in names){
									if(names[i].indexOf("seriya")!=-1){
										names.splice(i, 1);
										break;
									}
								}
								self.lastProducts[r].type_name = names.join(" ");
								break;
							}
						}
						self.lastProducts[r].images = JSON.parse(self.lastProducts[r].images);
						for(let j in self.lastProducts[r].colors){
							let key = false;
							for(let m in self.colors){
								if(self.colors[m].id == self.lastProducts[r].colors[j].id){
									self.colors[m].count++;
									key = true;
									break;
								}
							}
							 if(!key) {
								 self.colors.push(self.lastProducts[r].colors[j]);
									self.colors[self.colors.length-1].count = 1;
							}
						}
					}
					
					for(let i in self.currentBrendsO){
						self.currentBrends.push(self.currentBrendsO[i]);
					}
					self.products = data;
					
					var s = document.createElement("script");
					s.type = "text/javascript";
					s.src = "assets/js/electro.js";
					self.elementRef.nativeElement.appendChild(s);

						jQuery( ".price_slider" ).slider({
							animate: "fast",
							range: true,
							step: 1,
							min: self.minPrice,
							max: self.maxPrice,
							values: [self.minPrice, self.maxPrice],
							change: function( event, ui ) {
								console.log(event, ui);
								self.minPrice = ui.values[0];
								self.maxPrice = ui.values[1];
							}
						});
						jQuery( ".full-layout" ).hide();
					}, function errorCallback(error){
						console.log(error);
					});
				}
			}
		}
	
		selectSort(): void{
			console.log(this.selectedSort);
			let url, currentUrl;
			if(window.location.href.indexOf("?") != -1){
				if(window.location.href.indexOf("?sort") == -1){
					url = "&sort=" + this.selectedSort.value;
					if(window.location.href.split('&sort').length>1)
						currentUrl = window.location.href.split('&sort')[0];
					else
						currentUrl = window.location.href;
				} else {
					url = "?sort=" + this.selectedSort.value;
					if(window.location.href.indexOf("&show") != -1)
						url += window.location.href.substr(window.location.href.indexOf('&show'));
					currentUrl = window.location.href.split('?sort')[0];
				}
			}
			else{
				url = "?sort=" + this.selectedSort.value;
				currentUrl = window.location.href;
			}
			
			window.location.href = currentUrl + url;
		}
	
		selectShow(): void{
			console.log(this.selectedShow);
			
			let url, currentUrl;
			if(window.location.href.indexOf("?") != -1){
				if(window.location.href.indexOf("?show") == -1){
					url = "&show=" + this.selectedShow.value;
					if(window.location.href.split('&show').length>1)
						currentUrl = window.location.href.split('&show')[0];
					else
						currentUrl = window.location.href;
				} else {
					url = "?show=" + this.selectedShow.value;
					if(window.location.href.indexOf("&sort") != -1)
						url += window.location.href.substr(window.location.href.indexOf('&sort'));
					currentUrl = window.location.href.split('?show')[0];
				}
			}
			else{
				url = "?show=" + this.selectedShow.value;
				currentUrl = window.location.href;
			}
			
			window.location.href = currentUrl + url;
		}
	
		filter(): void{
			let self = this;
			
			let data: any = {};
	
			if(self.currentCategory)
				data.model_id = self.currentCategory;
	
			for(let a in self.currentBrends){
				console.log(self.currentBrends[a]);
				if(self.currentBrends[a].isSelected){
					if(!data.firm_ids)
						data.firm_ids = [];
					data.firm_ids.push(self.currentBrends[a].id);
				}
			}
//	
//			for(let a in self.colors){
//				if(self.colors[a].isSelected){
//					if(!data.colors_ids)
//						data.colors_ids = [];
//					data.colors_ids.push(self.colors[a].id);
//				}
//			}
	
			data.minPrice = self.minPrice;
			data.maxPrice = self.maxPrice;
			data.sort = self.selectedSort.value;
			data.show = self.selectedShow.value;
			
			console.log(data);
			self.eService.getFilter(data)
					.then(function successCallback(res){
						console.log(res);
						let data = res.allProducts.data;
				
						self.lastPage = res.allProducts.last_page;
						self.currentPage = res.allProducts.current_page;
						self.prevPageUrl = res.allProducts.prev_page_url;
						self.nextPageUrl = res.allProducts.next_page_url;
						self.lastPageUrl = res.allProducts.last_page_url;
						self.fromProd = res.allProducts.from;
						self.toProd = res.allProducts.to;
						self.totalProd = res.allProducts.total;
						self.banner = res.banner[0];
				
						for(let r in data){
							for(let m in self.models){
								if(data[r].model_id == self.models[m].id){
									data[r].model_name = self.models[m].name;
									break;
								}
							}
							for(let m in self.brends){
								if(data[r].firm_id == self.brends[m].id){
									data[r].firm_name = self.brends[m].name;
									break;
								}
							}
							for(let m in self.types){
								if(data[r].type_id == self.types[m].id){
									let names = self.types[m].name.split(" ");
									for(let i in names){
										if(names[i].indexOf("seriya")!=-1){
											names.splice(i, 1);
											break;
										}
									}
									data[r].type_name = names.join(" ");
									break;
								}
							}
							data[r].images = JSON.parse(data[r].images);
							for(let j in data[r].colors){
								let key = false;
								for(let m in self.colors){
									if(self.colors[m].id == data[r].colors[j].id){
										self.colors[m].count++;
										key = true;
										break;
									}
								}
								 if(!key) {
									 self.colors.push(data[r].colors[j]);
										self.colors[self.colors.length-1].count = 1;
								}
							}
							for(let m in data[r].reviews){
								if(data[r].reviews[m].stars != null && data[r].reviewsCount == undefined){
										data[r].reviewsCount = 1;
										data[r].reviewsA = data[r].reviews[m].stars;
									}
									else if(data[r].reviews[m].stars != null){
										data[r].reviewsCount++;
										data[r].reviewsA += data[r].reviews[m].stars;
									}
								}
								
								if(data[r].reviewsCount != undefined){
									data[r].reviewsA = parseInt(data[r].reviewsA)/data[r].reviewsCount;
									data[r].reviewsAw = data[r].reviewsA/5*100 + "%";
								} else {
									data[r].reviewsA = 0;
									data[r].reviewsAw = 0 + "%";
								}
							}
				

						self.products = data;
						
					}, function errorCallback(err){
						console.log(err);
					});
		}
		
		addToCart(product): void{
			let self = this;
			if(this.isLogged == 'true'){
				let total = 0;
				if(product.is_discount)
					total = product.new_price;
				else
					total = product.price;
				self.eService.addCart({userid: self.userid, productid: product.id, count: 1, total: total})
					.then(function successCallback(res){
						console.log(res);
						if(res.id){
							window.location.href = "/cart";
						}
					}, function errorCallback(err){
						console.log(err);
					});
			} else {
				window.location.href = "/account";
			}
		}
		
		paginate(url): void{
			let self = this;
			let data: any = {};
			data.minPrice = self.minPrice;
			data.maxPrice = self.maxPrice;
			data.sort = self.selectedSort.value;
			data.show = self.selectedShow.value;
			data.model_id = self.currentCategory; 
			data.type_id = self.currentModel; 
			data.brend_id = self.currentBrend;
			self.eService.paginate(data, url)
									.then(function successCallback(res){
											console.log(res);
			
											let data = res.allProducts.data;
				
											self.lastPage = res.allProducts.last_page;
											self.currentPage = res.allProducts.current_page;
											self.prevPageUrl = res.allProducts.prev_page_url;
											self.nextPageUrl = res.allProducts.next_page_url;
											self.lastPageUrl = res.allProducts.last_page_url;
											self.fromProd = res.allProducts.from;
											self.toProd = res.allProducts.to;
											self.totalProd = res.allProducts.total;

											for(let r in data){
												for(let m in self.models){
													if(data[r].model_id == self.models[m].id){
														data[r].model_name = self.models[m].name;
														break;
													}
												}
												for(let m in self.brends){
													if(data[r].firm_id == self.brends[m].id){
														data[r].firm_name = self.brends[m].name;
														break;
													}
												}
												for(let m in self.types){
													if(data[r].type_id == self.types[m].id){
														let names = self.types[m].name.split(" ");
														for(let i in names){
															if(names[i].indexOf("seriya")!=-1){
																names.splice(i, 1);
																break;
															}
														}
														data[r].type_name = names.join(" ");
														break;
													}
												}
												data[r].images = JSON.parse(data[r].images);
												for(let j in data[r].colors){
													let key = false;
													for(let m in self.colors){
														if(self.colors[m].id == data[r].colors[j].id){
															self.colors[m].count++;
															key = true;
															break;
														}
													}
													 if(!key) {
														 self.colors.push(data[r].colors[j]);
															self.colors[self.colors.length-1].count = 1;
													}
												}
										}


											self.products = data;
									}, function errorCallback(err){
											console.log(err);
									});
		}
}
