import { Component, Input, IterableDiffers, IterableDiffer, OnInit, DoCheck, ElementRef} from '@angular/core';
import { ElectroService } from '../electro.service';
import { NgForOf } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/Rx';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.css']
})
export class ShopDetailComponent implements OnInit, DoCheck {
	
		@Input() brends:any;
		@Input() models:any;
		@Input() types:any;
	
		differ:IterableDiffer<{}>;
	
		constructor(private route: ActivatedRoute, private differs: IterableDiffers, private elementRef:ElementRef, private eService: ElectroService, private cookieService: CookieService) {
			this.differ = differs.find([]).create(null);
		}
	
		quantity = 1;
		rating = -1;
	
		isLogged = this.cookieService.get("loggedin");
		userid = this.cookieService.get("userid");
		username = this.cookieService.get("name");
		surname = this.cookieService.get("surname");
		formUsername = '';
	
		productID: any = null;
		currentProduct: any = {};
		currentColor: any = '';
		currentColorID = null;
		relatedProducts = [];
		accessiores = [];
		
		currentURL = "";
	
		reviews = [];
		comment = '';
		ratingByNum: any = {
			1: 0,
			2: 0,
			3: 0,
			4: 0,
			5: 0
		};
		
		ngOnInit() {
			 	this.productID = (this.route.params as any).value.num;
			
				let color: any = "#" + (this.route.params as any).value.color;

				if((this.route.params as any).value.color)
					this.currentColor = color;
			
				let pathname = window.location.pathname.split("/");
				if(pathname.length>4)
					this.currentURL = "http://unimax.az/shop.php/" + pathname[2] + "/" + pathname[3] + "/" + pathname[4];
				else 
					this.currentURL = "http://unimax.az/shop.php/" + pathname[2] + "/" + pathname[3];
		}
	
		ngDoCheck() {
			let self = this;
			const change = this.differ.diff(this.models);
			if(change != null){
				self.currentProduct["imagesByColors"] = {};
				self.eService.getProduct({id: self.productID}).then(function successCallback(res){
					console.log(res);
					self.currentProduct = res.product;
					self.relatedProducts = res.related;
					self.reviews = res.reviews;
					for(var a in res.accessories){
						res.accessories[a].images = JSON.parse(res.accessories[a].images);
					}
					self.accessiores = res.accessories;
					self.currentProduct.images = JSON.parse(self.currentProduct.images);
					for(let m in self.models){
							if(self.currentProduct.model_id == self.models[m].id){
								self.currentProduct.model_name = self.models[m].name;
								break;
							}
						}
						for(let m in self.brends){
							if(self.currentProduct.firm_id == self.brends[m].id){
								self.currentProduct.firm_name = self.brends[m].name;
								break;
							}
						}
						for(let m in self.types){
							if(self.currentProduct.type_id == self.types[m].id){
								self.currentProduct.type_name_full = self.types[m].name;
								let names = self.types[m].name.split(" ");
								for(let i in names){
									if(names[i].indexOf("seriya")!=-1){
										names.splice(i, 1);
										break;
									}
								}
								self.currentProduct.type_name = names.join(" ");
								break;
							}
						}
					
					for(let i in self.currentProduct.colors){
						self.currentProduct.colors[i].colorID = self.currentProduct.colors[i].code.substr(1);
					}
					
					for(let r in self.relatedProducts){
						for(let m in self.models){
							if(self.relatedProducts[r].model_id == self.models[m].id){
								self.relatedProducts[r].model_name = self.models[m].name;
								break;
							}
						}
						for(let m in self.brends){
							if(self.relatedProducts[r].firm_id == self.brends[m].id){
								self.relatedProducts[r].firm_name = self.brends[m].name;
								break;
							}
						}
						for(let m in self.types){
							if(self.relatedProducts[r].type_id == self.types[m].id){
								let names = self.types[m].name.split(" ");
								for(let i in names){
									if(names[i].indexOf("seriya")!=-1){
										names.splice(i, 1);
										break;
									}
								}
								self.relatedProducts[r].type_name = names.join(" ");
								break;
							}
						}
						self.relatedProducts[r].images = JSON.parse(self.relatedProducts[r].images);
					}
					
					let imagesByColors = {};
					
					for(let i in self.currentProduct.images){
						let color = "#" + self.currentProduct.images[i].split("/")[2].substr(0,6);
						let key = false;
						for(let c in imagesByColors){
							if(color == c){
								key = true;
								imagesByColors[c].push(self.currentProduct.images[i]);
								break;
							}
						}
						if(!key){
							imagesByColors[color] = [self.currentProduct.images[i]];
						}
					}
					self.currentProduct.reviewsCount = self.currentProduct.reviews.length;
					let reviewStars = 0, starsCount = 0;
					for(let i in self.currentProduct.reviews){
						if(self.currentProduct.reviews[i].stars){
							reviewStars += parseInt(self.currentProduct.reviews[i].stars);
							starsCount++;
							self.ratingByNum[self.currentProduct.reviews[i].stars]++;
						}
					}
					for(let r in self.ratingByNum){
						self.ratingByNum[r + "w"] = Math.round(self.ratingByNum[r]/starsCount * 10000)/100 + "%";
					}
					if(reviewStars != 0){
						self.currentProduct.reviewStars = Math.round(reviewStars/starsCount * 100)/100;
						self.currentProduct.reviewRating = Math.round(self.currentProduct.reviewStars/5 * 10000)/100 + "%";
					}
					else{
						self.currentProduct.reviewStars = 0;
						self.currentProduct.reviewRating = 0;
					}
					
					if(self.currentColor == '')
						self.currentColor = Object.keys(imagesByColors)[0];
					self.currentProduct.imagesByColors = imagesByColors;
					
					self.currentProduct.desc_text = self.currentProduct.desc_text.split("<p>&nbsp;</p>");
					let textDesc = [], textI: any;
					for(textI in self.currentProduct.desc_text){
						if(textI==0)
							textDesc.push(self.currentProduct.desc_text[textI]);
						if(textI%2 == 1 && self.currentProduct.desc_text[parseInt(textI)+1] != undefined){
							textDesc.push(self.currentProduct.desc_text[textI].concat(self.currentProduct.desc_text[parseInt(textI)+1]));
						} else if(textI%2 == 1 && self.currentProduct.desc_text[parseInt(textI)+1] == undefined){
							textDesc.push(self.currentProduct.desc_text[textI]);
						}
					}
					self.currentProduct.desc_text = textDesc;
					self.currentProduct.desc_images = JSON.parse(self.currentProduct.desc_images);
					console.log(self.currentProduct, self.currentColor);
					
					for(let r in self.reviews){
						if(self.reviews[r].stars)
							self.reviews[r].width = (parseInt(self.reviews[r].stars)/5)*100 + '%';
						self.reviews[r].created_at = self.reviews[r].created_at.substr(0, 10);
					}
					
					for(let i in self.currentProduct.colors){
						if(self.currentProduct.colors[i].code == self.currentColor){
							self.currentColorID = self.currentProduct.colors[i].id;
							break;
						}
					}
					
					var s = document.createElement("script");
					s.type = "text/javascript";
					s.src = "assets/js/electro.js";
					self.elementRef.nativeElement.appendChild(s);
					
					jQuery( ".full-layout" ).hide();
				}, function errorCallback(error){
					console.log(error);
				});
			}
		}
	
		selectColor(code): void{
			this.currentColor = code;
			let self = this;
					
		}
	
		setRating(r): void{
			this.rating = r;
		}
	
		sendReview(): void{
			let self = this;
			if(self.isLogged=='true' && self.comment){
				self.eService.sendReview({stars: self.rating, review: self.comment, userid: self.userid, productid: self.currentProduct.id})
										.then(function successCallback(res){
												console.log(res);
											if(res.id){
												window.location.href = window.location.href + "#reviews";
												window.location.reload();
											}

										}, function errorCallback(err){
											console.log(err);
										});
			}	else if(self.isLogged!='true' && self.comment && self.formUsername) {
				self.eService.sendReview({stars: self.rating, review: self.comment, username: self.formUsername, productid: self.currentProduct.id})
										.then(function successCallback(res){
												console.log(res);
											if(res.id){
												window.location.href = window.location.href + "#reviews";
												window.location.reload();
											}

										}, function errorCallback(err){
											console.log(err);
										});
			}
		}
	
		addToCartSingle(product): void{
			let self = this;
			if(this.isLogged == 'true' && self.quantity>0){
				let total = 0;
				if(product.is_discount)
					total = product.new_price*self.quantity;
				else
					total = product.price*self.quantity;
				self.eService.addCart({userid: self.userid, productid: product.id, color_id: self.currentColorID, count: self.quantity, total: total})
					.then(function successCallback(res){
						console.log(res);
						if(res.id){
							window.location.href = "/cart";
						}
					}, function errorCallback(err){
						console.log(err);
					});
			} else if(this.isLogged == 'true') {
				window.location.href = "/account";
			}
		}
		
		addToCart(product): void{
			let self = this;
			if(this.isLogged == 'true'){
				let total = 0;
				if(product.is_discount)
					total = product.new_price;
				else
					total = product.price;
				self.eService.addCart({userid: self.userid, productid: product.id, count: 1, total: total})
					.then(function successCallback(res){
						console.log(res);
						if(res.id){
							window.location.href = "/cart";
						}
					}, function errorCallback(err){
						console.log(err);
					});
			} else {
				window.location.href = "/account";
			}
		}
		
		showReviewsTab(): void{
			let self = this;
			jQuery(".woocommerce-tabs .reviews_tab a").click();
			let top = jQuery('#reviews').position().top;
			jQuery(window).scrollTop( top );			
		}

}
