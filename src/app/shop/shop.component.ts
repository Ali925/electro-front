import { Component, ElementRef } from '@angular/core';
import { ElectroService } from '../electro.service';
import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

declare var jQuery:any;
declare var $ :any;

@Component({
	selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent {
	
	brends = [];
	models = [];
	types = [];
	blogs = [];
	pageNumber: any = null;
	
	constructor(private eService: ElectroService, private elementRef:ElementRef,private route: ActivatedRoute) {}
	
	ngOnInit(): void {
		let self = this;
		this.pageNumber = (this.route.data as any).value.page;
		this.eService.getStatus()
			.then(function succesCallback(data){
				let body = data;
				self.brends = body.allFirms;
				self.blogs = body.allBlogTitles;
				self.models = body.allModels;
				self.types = body.allTypes;
				for(let i=0;i<body.allTypes.length;i++){
					 for(let j in self.brends){
							if(self.brends[j].id == body.allTypes[i].firm_id){
								for(let k in self.brends[j].models){
									if(self.brends[j].models[k].id == body.allTypes[i].model_id){
												if(self.brends[j].models[k].types == undefined)
													self.brends[j].models[k].types = [];
												self.brends[j].models[k].types.push(body.allTypes[i]);
												break;
	}	
	}
	break;
	}
						}
				}		
				console.log('data: ', data);
				
		},function errorCallback(error){
			console.log(error);
		});
	}
	
}
