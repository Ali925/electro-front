import { Component } from '@angular/core';

@Component({
	selector: 'app-about',
  template: `
		<style>.full-layout{
	  position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    background-color: white;
		background-image: url(../../assets/images/anime.png);
	  background-repeat: no-repeat;
    background-position: center;
    z-index: 10000;
}</style>
    <div class="full-layout"></div>
  `
})
export class SpinComponent {

}
